const collection = require('lodash/collection')
const getPostOptions = (city, checkin, checkout, provider) => {
      return {
      method: 'POST',
      uri: 'https://experimentation.getsnaptravel.com/interview/hotels',
      body: {
        city: city,
        checkin: checkin,
        checkout: checkout,
        provider: provider
      },
      json: true,
    }
}

module.exports = {
  getSnaptravelPostOptions: (city, checkin, checkout) => {
    return getPostOptions(city, checkin, checkout, 'snaptravel')
  },
  getHotelsPostOptions: (city, checkin, checkout) => {
    return getPostOptions(city, checkin, checkout, 'retail')
  },
  getPrices: (common, hotelsResults, snaptravelResults) => {
    prices = []
    common.forEach((hotel) => {
      const id = hotel.id
      prices.push([
        collection.find(hotelsResults, ['id', id]).price,
        collection.find(snaptravelResults, ['id', id]).price
      ])
    })
    return prices
  }
}

const express = require('express')
const app = express()
const array = require('lodash/array')
const bodyParser = require('body-parser')
const cache = require('memory-cache')
const helpers = require('./helpers')
const rp = require('request-promise-native')

const port = 3000

// middleware
app.set('view engine', 'pug')
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// homepage
app.get('/', (req, res) => {
  res.render('index', { title: 'Heyo Snap Travel!', message: 'My nice form' })
})

app.post('/results', (req, res) => {
  // we set the cache key to be the query body stringified
  const cacheKey = JSON.stringify(req.body)
  const cachedResults = cache.get(cacheKey)

  // if the cache of the results exists, send the cached results
  if (cachedResults !== null) {
    return res.render('results', cachedResults)
  }

  // construct request objects
  const hotels = helpers.getHotelsPostOptions(
    req.body.city,
    req.body.checkin,
    req.body.checkout,
  )
  const snaptravel = helpers.getSnaptravelPostOptions(
    req.body.city,
    req.body.checkin,
    req.body.checkout,
  )

  // do all the requests in parallel
  Promise.all([rp(hotels), rp(snaptravel)])
    .then((values) => {
      // get the relevant info to display
      const hotelsResults = values[0].hotels
      const snaptravelResults = values[1].hotels
      const common = array.intersectionBy(
        hotelsResults,
        snaptravelResults,
        'id'
      )
      const prices = helpers.getPrices(common, hotelsResults, snaptravelResults)

      // cache the results for successive api calls
      const results = {results: common, prices}
      cache.put(cacheKey, results)

      // render the page
      res.render('results', results)
    })
})

app.listen(port, () => console.log(`Listening on port ${port}!`))
